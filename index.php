<?php

//inclusion bibliotheque de fonctions
include_once('php/lib_functions.php');

initiateStartConnect('php/lib_params.php', 'php/bib_fonctions.php');

htmlInit('Startblags - Accueil');

//$form_text = getPublicPageBandeauHtml($login_path);
//
//if ($page_publique) {
//	$html_to_show = $form_text;
//} else {
//	$html_to_show = $show_links ?
//		getDefaultLinks()
//		: '';
//}

showBandeauPagePublique();

showContenu();

showPied();

htmlEnd();

ob_end_flush();

function showContenu(){
	$sql1 = 'SELECT blTitre as titre, count(*) as compte
		FROM blogs, blogs_visites
		WHERE bvIDBlog = blID 
		GROUP BY blTitre
		ORDER BY compte DESC
		LIMIT 3';
		
	$sql2 = 'SELECT arTitre as titre, count(*) as compte
	FROM articles, articles_notes
	WHERE anIDArticle = arID 
	GROUP BY arTitre
	ORDER BY compte DESC
	LIMIT 3';
		
	echo'<div id="blcContenu">
			<div id="blcHitParade">
				<div id="blcTag"  width="250px">
				<h3>Tags [+]</h3>
				</div>
			</div>';
	showTable('Les 3 blogs les plus visit&eacute;s', $sql1);
	showTable('Les 3 articles les mieux not&eacute;s', $sql2);
	showListeBlogs();
	echo'</div>';
}

function showTable($title, $sql){
	$bd = $GLOBALS['bd'];
	echo'<br><table width="430px" border="1" bordercolor="#B90F0F" cellspacing="0" cellpadding="0" align="right">
				<tr> 
					<td colspan="2">
						<h3>',$title,'</h3>
					</td>
				</tr>';
	$result = mysqli_query($bd, $sql) or bdErreur($bd, $sql);
	while ($enr = mysqli_fetch_assoc($result)) {
		htmlProteger($enr);
		echo'<tr>
			<td>', $enr['titre'], '</td>', 
			'<td align="center"><div class="classement">', $enr['compte'], '</div></td>';
		echo'</tr>';
	}
	echo'</table>';
}

function showListeBlogs(){
	$bd = $GLOBALS['bd'];
	$sql='SELECT blID, blTitre, blResume, blAuteur, blDate, count(*) as compte, arDate
		  FROM blogs, articles
		  WHERE blID = arIDBlog
		  GROUP BY blTitre
		  ORDER BY blTitre ASC, arDate DESC';
	$result = mysqli_query($bd, $sql) or bdErreur($bd, $sql);
	while ($enr = mysqli_fetch_assoc($result)) {
		htmlProteger($enr);
		$dateStringBlog = $enr['blDate'];
		$dateStringArticle = $enr['arDate'];
		$dateBlog = formatDate($dateStringBlog);
		$dateArticle = formatDate($dateStringArticle);
		$blogID = urlencode(encryptAndHash($enr['blID']));
		echo'<div class="blcBlog">
			<h3>		
				<span class="blogAuteur">',"$enr[blAuteur] - $dateBlog","</span>
				$enr[blTitre]
			</h3><p>$enr[blResume]</p>",
			'<p class="petit">
			<a class="blogLienArticle" href="php/articles_voir.php?x=',
				$blogID,
				'" title="Voir les articles du blog">', 
	        	"$enr[compte] articles",
	        	"</a>- $dateArticle</p></div>";	
	}
}

?>
