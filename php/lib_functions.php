<?php
/* bibliothèque de fonctions */

/**
 * fichier racine contenant tous les fichier php sauf index.php
 */
$php_folder = 'php';

/** Affiche tous les elements du début d'une page html
 * @param string $title Titre de la page
 */
function htmlInit($title){
	$path = acquireCssPath();
	 echo '<!DOCTYPE html>',
        '<html>',
            '<head>',
                '<meta charset="ISO-8859-1">',
                '<link rel="stylesheet" href="',$path,'" type="text/css">',
                '<title>', $title, '</title>',
                '<script type="text/javascript" src="../js/bibli.js"></script></head><body>';
}

/**
 * Affiche les elements a la fin d'une page html
 */
function htmlEnd(){
	echo '</body></html>';
}

/**
 * Affiche le bandeau des pages, les chemins des fichiers sont choisis en fonction de la position du script
 * dans l'arborescence du projet.
 * @param $html_to_show
 */
function showBandeau($html_to_show){
	$index_path = acquireIndexPath();
	$images_path = acquireImagesPath();

	echo '<div id="blcBandeau">' .
		$html_to_show
		. '<a href="',$index_path,'">
			<img src="',$images_path,'" title="Accueil StarBlagS">
		</a></div>';
}

/**
 * @return string Code html pour le formulaire de login
 */
function getPublicPageBandeauHtml()
{
	$login_path = acquireLoginPath();
	return '<form method="post" action="' . $login_path . '" >
			<label for="txtPseudo">Pseudo</label>
			<input type="text" name="txtPseudo" id="txtPseudo" value="">
			<label for="txtPasse">Passe</label>
			<input type="password" name="txtPasse" id="txtPasse" value="">
			<input type="submit" name="btnLogin" value="Mon blog" class="bouton">
			<input type="submit" name="btnNouveau" value="Cr&eacute;er un blog" class="bouton">
		</form>';
}


/**
 * Affiche bandeau des pages publiques
 */
function showBandeauPagePublique()
{
	showBandeau(getPublicPageBandeauHtml());
}

/**
 * @return string
 */
function getDefaultLinks()
{
	return generateLinksWithText('article_maj.php', 'Nouvel article', 'articles_liste.php', 'Mes articles');
}

/** Cree code html avec les deux liens pour la bandeau
 * @param $link1
 * @param $text1
 * @param $link2
 * @param $text2
 * @return string
 */
function generateLinksWithText($link1, $text1, $link2, $text2)
{
	return '<div id="blcMajLiens">
				<a href="'.$link1.'">'.$text1.'</a><br><a href="'.$link2.'">'.$text2.'</a>
			</div>';
}

/**
 * Affiche le pied de page par defaut pour toutes les pages (dans celles ou il ya un pied de page)
 */
function showPied(){
	echo '<div id="blcPied">
		StarBlagS est une marque d&eacute;pos&eacute;e appartenant &agrave; la soci&eacute;t&eacute; PTICON(PiaT International Corporation Original Network)
		<br>
		<br>
		<a href="">Contactez-nous</a> -
		<a href="">Informations l&eacute;gales</a> -
		<a href="">Conditions ',"d'utilisation</a>
	</div>";
}

/**
 * Format de la date
 * @param string $dateFromSql date fournie par la requete sql
 * @return string date formattée selon le format DD/MM/YYYY
 */
function formatDate($dateFromSql){
	$date = substr($dateFromSql, -2).'/'.substr($dateFromSql, -4, 2).'/'.substr($dateFromSql, 0, 4);
	return $date;
}

/**
 * Fonction d'encryptage d'une chaine
 * @param string $s
 * @return null si string est null | string chaine encryptée si la fonction mcrypt_encrypt existe, la chaine
 * telle quelle sinon
 */
function encryptString($s){
	if (is_null($s)) {
		return null;
	}
	if (function_exists('mcrypt_encrypt')) {
		return rtrim(mcrypt_encrypt(MCRYPT_BLOWFISH, CLE_ENCRYPT_DECRYPT, $s, MCRYPT_MODE_ECB));
	} else {
		echo 'mcrypt_encrypt does not exist';
		return $s;
	}
}

/**
 * Fonction de decryptage d'une chaine
 * @param string $s chaine a decrypter
 * @return null si $s est null | string la chaine decryptée si la fonction mcrypt_decrypt existe, la chaine $s
 * telle quelle sinon
 */
function decryptString($s){
	if (is_null($s)) {
		return null;
	}
	if (function_exists('mcrypt_decrypt')) {
		return rtrim(mcrypt_decrypt(MCRYPT_BLOWFISH, CLE_ENCRYPT_DECRYPT, $s, MCRYPT_MODE_ECB));
	} else {
		echo 'mcrypt_decrypt does not exist';
		return $s;
	}
}

/**
 * Encrypte la chaine et lui rajoute son hash a la fin, utilise l'algorithme tiger160.
 * @param string $s
 * @return null si $s est null | string encrypteee et avec son hash
 */
function encryptAndHash($s){
	if (is_null($s)) {
		return null;
	}
	$s = encryptString($s);
	$s = $s . hash('tiger160,4', $s);
	//echo $string;
	return $s;
}

/** Verifie le hash de la chaine et si tout est correct retourne la chaine decryptée, utilise l'algorithme tiger160.
 * @param string $s
 * @return null si $s est null ou si mauvais hash | string : chaine decryptée
 */
function verifyHashAndDecrypt($s){
	if (is_null($s)) {
		return null;
	}
	$x = substr($s, 0, -40);
	$s = substr($s, -40);
	if (hash('tiger160,4', $x) != $s) {
		return null;
	} else {
		return decryptString($x);
	}
}

/**
 * Obtient le chemin du fichier en fonction du script en cours
 * @return string	chemin du fichier css/modele_1.css
 */
function acquireCssPath(){
	$script_path = getPathOneBeforeLastFolder();
	//echo $script_path;
	global $php_folder;
	if (substr_count($script_path, $php_folder) === 0) {
		return CSS_PATH_INDEX;
	} else {
		return CSS_PATH_OTHERS;
	}
}

/**
 * Obtient le chemin du fichier en fonction du script en cours
 * @return string	chemin du fichier php/login.php
 */
function acquireLoginPath(){
	$script_path = getPathOneBeforeLastFolder();
	global $php_folder;
	if (substr_count($script_path, $php_folder) === 0) {
		return LOGIN_PATH_INDEX;
	} else {
		return LOGIN_PATH_OTHERS;
	}
}

/**
 * Obtient le chemin du fichier en fonction du script en cours
 * @return string	chemin du fichier index.php
 */
function acquireIndexPath(){
	$script_path = getPathOneBeforeLastFolder();
	global $php_folder;
	if (substr_count($script_path, $php_folder) === 0) {
		return INDEX_PATH_INDEX;
	} else {
		return INDEX_PATH_OTHERS;
	}
}

/**
 * Obtient le chemin du fichier en fonction du script en cours
 * @return string	chemin du fichier images/logo.gif
 */
function acquireImagesPath(){
	$script_path = getPathOneBeforeLastFolder();
	global $php_folder;
	if (substr_count($script_path, $php_folder) === 0) {
		return IMAGES_PATH_INDEX;
	} else {
		return IMAGES_PATH_OTHERS;
	}
}

/**
 * Obtient le nom du dossier dans lequel se trouve le script
 * Ce resultat nous permterra de savoir s''il s'agit du fichier index.php ou d'un script du dossier php
 * et ainsi nous pourrons obtenir le chemin des autres fichiers utilisés dans ce script
 * @return string	dossier du scrypt en cours
 */
function getPathOneBeforeLastFolder(){
	$script_path = $_SERVER['PHP_SELF'];
	$arr = explode(DIRECTORY_SEPARATOR, $script_path);
	$size = count($arr);
	return $arr[$size-2];
}

/**
 * Renvoie a la page index.html
 */
function exitToHomePage(){
	$path = acquireIndexPath();
	header("Location: $path");
}

/**
 * Fonction d'erreur simplifiée (temporaire / en mode debug)
 * @param	string	$message 	message d'erreur ou chaine sql
 */
function tempBdError($message){
	echo "Erreur : $message";
	exit();
}

/**
 * Initialise le buffer, la session, ajoute les librairies a importer et connecte la base de données
 */
function initiateStartConnect($path_constants, $path_fonctions)
{
	ob_start();
	session_start();

	//inclusion fichier constantes
	include_once($path_constants);

	include_once($path_fonctions);
	bdConnecter();
}

/** Verifie si chaine vide ou nulle
 * @param string $s
 * @return bool
 */
function isValidString($s) {
	if(is_null($s) || strlen($s) == 0){
		return false;
	}
	return true;
}

/** Verifie si le mail est valide
 * @param string $mail adresse mail a verifier
 * @return bool
 */
function isValidMail($mail){
	if (!isValidString($mail)) {
		return false;
	}
	#caractère @
	$arobase_pos = strpos($mail, '@');
	if ($arobase_pos === false
		|| $arobase_pos != strrpos($mail, '@')
		|| $arobase_pos === 0) {
		return false;
	}

	$table1 = explode('@', $mail);
	$size = count($table1);
	if ($size !== 2) {
		return false;
	}

	for ($k = 0; $k < $size; $k++) {
		$part = $table1[$k];
		if (is_null($part) || !is_string_alphabetic(substr($part, 0, 1))) {
			return false;
		}
	}

	#caractère . et dernière partie
	if (strpos($mail, '.') === false) {
		return false;
	}

	$table = explode('.', $mail);
	$last_part = $table[count($table)-1];
	$string_length = strlen($last_part);
	if (strlen($string_length) > 4 || $string_length < 2) {
		return false;
	}

	if (!is_string_alphabetic($last_part)) {
		return false;
	}

	#caractères admis
	$table2 = str_split($mail);
	for ($j=0; $j < count($table2); $j++) {
		$char2 = ord($table2[$j]);
		if (! ($char2 == 45 || $char2 == 46 || $char2 == 95
			|| ($char2 >= 48 && $char2 <= 57)
			|| ($char2 >= 64 && $char2 <= 90)
			|| ($char2 >= 97 && $char2 <= 122)) ) {
			return false;
		}
	}
	return true;
}

/** La chaine ne contient que des caractères alphabetiques?
 * @param string $str chaine à verifier
 * @return bool
 */
function is_string_alphabetic($str){
	$table = str_split($str);
	for ($i = 0; $i < count($table); $i++ ){
		$char = ord($table[$i]);
		if ( ! ( ($char >= 64 && $char <= 90)
			|| ($char >= 97 && $char <= 122)) ) {
			return false;
		}
	}
	return true;
}

/** La chaine ne contient que des caractères alpha-numeriques?
 * @param string $str chaine à verifier
 * @return bool
 */
function is_string_alpha_numeric($str){
	$table = str_split($str);
	for ($i = 0; $i < count($table); $i++ ){
		$char = ord($table[$i]);
		if ( ! ( ($char >= 64 && $char <= 90)
			|| ($char >= 97 && $char <= 122) || is_numeric($char)) ) {
			return false;
		}
	}
	return true;
}

/** L'element login ou mot de passe sont ils valides?
 * critères: 4 a 10 caracteres alphanumeriques
 * @param string $str chaine a rechercher
 * @return bool
 */
function isValidLoginOrPass($str){
	if(strlen($str) < 4 || strlen($str) > 10){
		return false;
	} else if(!is_string_alpha_numeric($str)) {
		return false;
	}
	return true;
}

/**
 * @global array error_messages
 * @return string chaine contenant le code html avec toutes les erreurs
 */
function getErrorMessagesHtmlIfAny($error_messages) {
	$error_content = '';
	if (count($error_messages) > 0) {
		$error_content = '<div class=blcErreurs> Erreurs d&eacute;t&eacute;ct&eacute;es: ';
		foreach ($error_messages as $m) {
			$error_content.='<li>'.$m;
		}
		$error_content.='</div>';
	}
	return $error_content;
}

/**
 * Redirige vers la page index.php et appelle exit pour arreter l'execution du script
 */
function goToHomePageAndExit()
{
	exitToHomePage();
	exit();
}

/**
 * Genere code html des champs input
 * @param int $checkedValue
 * @param int $value
 * @param string $texte description du champ
 * @return string
 */
function getInputHtmlCode($checkedValue, $value, $texte, $name){
	$checked_html = $checkedValue === $value ? 'checked=true' : '';
	return '<input type="radio" name="' . $name . '" value="' .$value.'" '.$checked_html.'>'.$texte;
}
?>
