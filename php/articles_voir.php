<?php

//inclusion bibliotheque de fonctions
include_once('lib_functions.php');

initiateStartConnect('lib_params.php', 'bib_fonctions.php');

$blog_id = 1;
$x = '';

$nbArt = 1;
$first_article = 0;
$last_article = 0;
$page = 1;

if (isset($_GET['x'])) {
	$x=$_GET['x'];
	$xx = verifyHashAndDecrypt($x);
	if(is_null($xx)){
		//echo $x;
		//echo $xx;
		exitToHomePage();
	}
	//echo $xx;
	$blog_id = 0 + $xx;
	//echo "<br>blog_id = $blog_id<br>";
} else {
	echo 'x not set';
	exitToHomePage();
}


if(isset($_GET['f']) && estEntier($_GET['f'])){
	$first_article = (int) $_GET['f'];
	echo "<br>$first_article";
}

if(isset($_GET['l']) && estEntier($_GET['l'])){
	$last_article = (int) $_GET['l'];
	echo "<br>$last_article";
}

if(isset($_GET['p']) && estEntier($_GET['p'])){
	$page = (int) $_GET['p'];
	echo "<br>$page<br>";
}

if(isset($_GET['n']) && estEntier($_GET['n'])){
	$nbArticles = (int) $_GET['n'];
	//echo "<br>$page<br>";
}

$bd = $GLOBALS['bd'];

$sql = "SELECT blID, blTitre, blResume, blAuteur, blPhoto, blDate, bvDate, count(*) as nbVisites, blNbArticlesPage, blTri
		FROM blogs, blogs_visites
		WHERE blID = $blog_id
		ORDER BY bvDate DESC";
   	
$result_blogs = mysqli_query($bd, $sql) or bdErreur($bd, $sql);

//details du blog
$enr = mysqli_fetch_assoc($result_blogs);

htmlProteger($enr);
$nbArticlesParPage = $enr['blNbArticlesPage'];
$order = $enr['blTri'] == 0 ? 'ASC' : 'DESC';
     	
$sql2 = "SELECT arID, arIDBlog, arTitre, arTexte, arComment, arDate, arHeure, SUM(anNote) as notation
		FROM articles LEFT JOIN articles_notes ON anIDArticle = arID
		WHERE arIDBlog = $blog_id
		GROUP BY arID
		ORDER BY arDate $order";
		
if($first_article > 0){
	echo $first_article,' ', $nbArticlesParPage;
	$sql2 .=" LIMIT $first_article, $nbArticlesParPage";
}
     	
$result_articles = mysqli_query($bd, $sql2) or bdErreur($bd, $sql2);

//liste d'articles
$enr2 = mysqli_fetch_assoc($result_articles);

$titre_blog = $enr['blTitre'];
if($first_article == 0){
	$nbArticles = mysqli_num_rows($result_articles);
	$first_article = 1;
}
$publication_arg = $enr['blTri'] == 0 ? 'premi&egrave;re' : 'derni&egrave;re';

htmlInit("Startblags - $titre_blog");

showBandeauPagePublique();

$image_url = "../upload/$enr[blID].$enr[blPhoto]";

/** Montre le contenu principal de la page
 * @param string $titre_blog
 * @param string $image_url image du blog
 * @param array $enr resultat de la recherche du blog
 * @param integer $nbArticles
 * @param string $publication_arg
 * @param array $enr2 resultat de la recherche d'articles d'un blog
 */
function showContenu($titre_blog, $image_url, $enr, $nbArticles, $publication_arg, $enr2)
{
	echo '<div id="blcContenu">
		<div class="blcBlog">
			<h1>', $titre_blog, '</h1>
			<img src="', $image_url, '" hspace="5" align="right">
			<ul>
				<li style="margin-bottom: 12px;">', $enr['blResume'], '</li>
				<li> Auteur : ', $enr['blAuteur'], '</li>
				<li>Nombre de visites : ', $enr['nbVisites'], ' depuis le ', formatDate($enr['blDate']), "</li>
				<li>Nombre d'articles : $nbArticles ($publication_arg publication le ", formatDate($enr2['arDate']),
	')<a href=""class="blogLienListe" title="Afficher la liste des articles"></a>
				</li>
			</ul>
		</div>';
}

showContenu($titre_blog, $image_url, $enr, $nbArticles, $publication_arg, $enr2);

//afficher premier article
showArticle($enr2);
$nbArt += 1;
while ($enr2 = mysqli_fetch_assoc($result_articles)) {
	if($nbArt <= $nbArticlesParPage){
		showArticle($enr2);
		$nbArt += 1;
	} else {
		$last_article = $nbArt;
		break;
	}
}

showPagination($first_article, $nbArticles);
echo '</div></div>';

showPied();

htmlEnd();
ob_end_flush();

/** Montre les articles avec la pagination
 * @param integer $first_article id du premier article visible
 * @param integer $nbArticles nombre total d'articles du blog
 */
function showPagination($first_article, $nbArticles){
	global $blog_id, $page, $nbArticlesParPage;
	$last_article = $first_article + $nbArticlesParPage - 1;
	$last_article = $last_article > $nbArticles ? $nbArticles : $last_article;
	echo '<div id="blcPagination">Articles ',$first_article," &agrave; $last_article sur $nbArticles <br>Page ";
	$nbPages = getNumberOfPages($nbArticles, $nbArticlesParPage);
	for($i = 1 ; $i <= $nbPages; $i++){
		if($i == $page){
			echo '<span id="pageEnCours">',$i,'</span>';
		} else {
			$new_first = 1 + (($i-1) * $nbArticlesParPage);
			$new_first = $new_first > $nbArticles ? $nbArticles : $new_first;
			$new_last = $new_first + $nbArticlesParPage;
			$new_last = $new_last > $nbArticles ? $nbArticles : $new_last;
			echo '<a href="articles_voir.php?x=',urlencode(encryptAndHash($blog_id)),"&f=$new_first&l=$new_last&p=$i&n=$nbArticles",'">',$i,'</a>';
		}
	}
	echo '</div>';
}

/** Affiche un article
 * @param array $enr array contenant les donnés d'un article
 */
function showArticle($enr){
	global $x, $bd;
	$date = formatDate($enr['arDate']);
	$nbCommentaires = ($enr['arComment']) === 0 ? 'Pas de' : $enr['arComment'];
	$artID = $enr['arID'];
	$titre = $enr['arTitre'];
	$note = $enr['notation'] === null ? 'Pas de note' : $enr['notation'];
	$photo_en_haut = null;
	$photo_a_droite = null;
	$photo_en_dessous = null;
	$photo_a_gauche = null;
	$sql = "SELECT phID, phNumero, phPlace, phLegende, phExt
			FROM photos
			WHERE phIDArticle = $artID";
	$result = mysqli_query($bd, $sql) or bdErreur($bd, $sql);
	while($enr1 = mysqli_fetch_assoc($result)){
		htmlProteger($enr1);
		$photo_full_url = "../upload/$artID".'_'."$enr1[phNumero].$enr1[phExt]";
		switch($enr1['phPlace']){
			case 0:
			$photo_en_haut = $photo_full_url;
			break;
			
			case 1:
			$photo_a_droite = $photo_full_url;
			break;
			
			case 2:
			$photo_en_dessous = $photo_full_url;
			break;
			
			case 3:
			$photo_a_gauche = $photo_full_url;
			break;
		}
	}
	
	$html_code_photo_haut = getPHotoHtmlCode($photo_en_haut);
	$html_code_photo_droite = getPHotoHtmlCode($photo_a_droite);
	$html_code_photo_dessous = getPHotoHtmlCode($photo_en_dessous);
	$html_code_photo_gauche = getPHotoHtmlCode($photo_a_gauche);
	
	$comment_url = $nbCommentaires == 0 ? '""' : '"javascript:FP.ouvrePopUp('."'comments_voir.php?x=$artID&y=$titre')".'"';
	
	echo'<div class="blcArticle">
			<h2>
				<span class="articleDate">',$date,' - ',$enr['arHeure'],'</span>
					<a href="article_maj.php?x=',$x,'">',$enr['arTitre'],"</a>		
			</h2>
			<br><br>
			<table>
			<tr>$html_code_photo_haut</tr> 
			<tr><td>$html_code_photo_gauche</td><td>$enr[arTexte]</td><td>$html_code_photo_droite</td></tr>
			<tr>$html_code_photo_dessous</tr> 
			</table>",
			'<div class="blcLiens">',
				'<a href=',$comment_url,' class="articleLienCom">',$nbCommentaires,' commentaires</a>
				<a href="" class="articleLienComAjout">ajouter un commentaire</a>
				<a class="articleNote">',$note,'</a>
				<a href="" class="articleLienNoteAjout">noter</a>
			</div>	
		</div>';
}

/** Renvoie le code html complet pour afficher une image
 * @param string $url de la photo
 * @return string code html
 */
function getPhotoHtmlCode($url){
	if($url === null || $url === ''){
		return '';
	} else {
		return '<img src="'.$url.'">';
	}
}

/** Calcule le nombre de pages necessaires pour
 * @param $nbArticles
 * @param $nbArticlesParPage
 * @return float
 */
function getNumberOfPages($nbArticles, $nbArticlesParPage){
	if($nbArticles % $nbArticlesParPage == 0){
		return (int) $nbArticles / $nbArticlesParPage;
	} else {
		return ((int) $nbArticles / $nbArticlesParPage) + 1;
	}
}

?>
