<?php
//inclusion bibliotheque de fonctions
include_once('lib_functions.php');

initiateStartConnect('lib_params.php', 'bib_fonctions.php');

htmlInit('Startblags - Article');
if (!isset($_SESSION['update'])){
    echo 'update not set';
    //erreur on est pas passé par login.php
    goToHomePageAndExit();
}
$error_messages = array();
if (isset($_GET['x'])) {
    $x = $_GET['x'];
    $xx = verifyHashAndDecrypt($x);
    if(!is_null($xx)){
        $_SESSION['arID'] = 0 + $xx;
    }
} else {
    unset($_SESSION['arID']);
}
$html_to_show = $_SESSION['update'] ?
    generateLinksWithText('blog_maj.php', 'Mon blog', 'articles_liste.php', 'Mes articles') : '';
$arTitre = $arTexte = '';
$arComment = 1;

showBandeau($html_to_show);

verifyFieldsAndUpdateDb($error_messages);

showContenu();
showPied();

htmlEnd();

ob_end_flush();

/**
 * Montre le contenu du formulaire de saisie et des erreurs eventuelles
 */
function showContenu(){
    global $error_messages, $arTexte;
    $is_update = isset($_SESSION['arID']) && $_SESSION['arID'] != -1;
    $titre = $is_update ? 'Je modifie un article...' : 'Je cr&eacute;e un article...';
    $bloc_html_erreurs = getErrorMessagesHtmlIfAny($error_messages);

    echo '<div id="blcContenu">
		        <div id="blcMajTitre">'
                    .$titre.
                '</div>'.$bloc_html_erreurs.
                    '<form method="post" action="article_maj.php">
                        <table class="majTable">';
                            showFieldTitre();
                            showFieldTexte();
                            showRadio();
                            showButtons();
    echo '</table></form></div>';

}

/**
 * Affiche le champ arTitre et initialise la valeur si necessaire
 */
function showFieldTitre(){
    global $arTitre;
    echo '<tr><td>Titre article</td><td><input type="text" name="arTitre" class="saisie" size="80" value="'.$arTitre.'"></td></tr>';
}

/**
 * Affiche le champs arTexte et initialise la valeur si necessaire
 */
function showFieldTexte(){
    global $arTexte;
    echo '<tr><td>Texte</td><td><textarea name="arTexte" class="saisie" cols="80" rows="15">'.$arTexte.'</textarea></td></tr>';
}

/**
 * Affiche le champs arComment et initialise la valeur si necessaire
 */
function showRadio(){
    global $arComment;
    $getInputHtmlCode1 = getInputHtmlCode($arComment, 0, 'Interdits','arComment');
    $getInputHtmlCode2 = getInputHtmlCode( $arComment, 1, 'Permis', 'arComment');
    echo '<tr><td>Commentaires</td>
            <td>'.$getInputHtmlCode1.'<br>'. $getInputHtmlCode2 .'</td></tr>';
}

/**
 * Affiche les boutons
 */
function showButtons(){
    $is_update = isset($_SESSION['arID']) && $_SESSION['arID'] != -1;
    $extra_button_html = $is_update  ? getButtonHtmlCode('btnSupprimer', 'Supprimer') : '<td></td>';
    echo "<tr>$extra_button_html".getButtonHtmlCode('btnValider', 'Valider').'</tr>';
}

/**
 * Code html pour afficher un bouton
 * @param string $name la valeur pour le champ name
 * @param string $value la valeur pour la champ value
 * @return string le code html
 */
function getButtonHtmlCode($name, $value)
{
    return '<td align="right"><input type="submit" name="'.$name.'" value="'.$value.'" class="bouton"></td>';
}

/**
 * Verifie si supprimer ou valider ont ete appelees et appele les fonctions respectives
 */
function verifyFieldsAndUpdateDb()
{
    if (isset($_POST['btnValider'])) {
        insertOrUpdateArticle();
    } else if(isset($_POST['btnSupprimer'])){
        deleteArticle();
    } else {
        updateVarIfNeeded();
    }
}

/**
 * Si modification d'article, on extrait les donnees de la bd pour initialiser les champs.
 */
function updateVarIfNeeded()
{
    global $arTitre, $arTexte, $arComment;
    if (isset($_SESSION['arID']) && $_SESSION['arID'] != -1) {
        $sql = "SELECT arTitre, arTexte, arComment
                FROM articles
                WHERE arID=$_SESSION[arID]";
        $result = mysqli_query($GLOBALS['bd'], $sql) or bdErreur($GLOBALS['bd'], $sql);
        $enr = mysqli_fetch_assoc($result);
        htmlProteger($enr);
        $arTitre = $enr['arTitre'];
        $arTexte = $enr['arTexte'];
        $arComment = (int)$enr['arComment'];
    }
}

/**
 * Verifie les champs du formulaire et insere un nouveau blog ou modifie un blog existant
 */
function insertOrUpdateArticle()
{
    global $arTitre, $arTexte, $arComment, $error_messages;
    $arTitre = $_POST['arTitre'];
    $arTexte = $_POST['arTexte'];
    $arComment = (int)$_POST['arComment'];
    if (!isValidString($arTitre)) {
        $error_messages [] = 'Le titre ne peut etre vide.';
    } else {
        $arTitre = mysqli_real_escape_string($GLOBALS['bd'], $arTitre);
    }
    if (!isValidString($arTexte)) {
        $error_messages [] = 'Le texte ne peut etre vide.';
    } else {
        $arTexte = mysqli_real_escape_string($GLOBALS['bd'], $arTexte);
    }
    if ($arComment > 1 || $arComment < 0) {
        $error_messages [] = 'Le champs commentaires est invalide.';
    }
    if (count($error_messages) == 0) {
        $sql = "arTitre='{$arTitre}', arTexte='{$arTexte}', arComment=$arComment";
        $is_update = isset($_SESSION['arID']) && $_SESSION['arID'] != -1;
        if ($is_update) {
            //update
            $sql = "UPDATE articles SET $sql WHERE arID=$_SESSION[arID] ";
        } else {
            $date = date("Ymd");
            $heure = date("H:m");
            $sql = "INSERT INTO articles
                    SET $sql, arPublier=1, arRSS=1, arDate='{$date}', arHeure='{$heure}', arIDBlog=$_SESSION[blID]";
        }
        $result = mysqli_query($GLOBALS['bd'], $sql) or bdErreur($GLOBALS['bd'], $sql);
        if(!$is_update){
            $_SESSION['arID'] = mysqli_insert_id($GLOBALS['bd']);
        }
        $_SESSION['uploadType'] = UPLOAD_TYPE_ARTICLE;
        header("Location: upload.php");
        exit();
    }
}

/**
 * Efface un article
 */
function deleteArticle(){
    global $error_messages;
    $is_update = isset($_SESSION['arID']) && $_SESSION['arID'] != -1;
    if($is_update){
        $sql = "DELETE FROM articles WHERE arID=$_SESSION[arID]";
        $result = mysqli_query($GLOBALS['bd'], $sql) or bdErreur($GLOBALS['bd'], $sql);
        unset($_SESSION['arID']);
    } else {
        $error_messages[]="Pas d'article &agrave supprimer";
    }
}
?>