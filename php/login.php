<?php
//inclusion bibliotheque de fonctions
include_once('lib_functions.php');

initiateStartConnect('lib_params.php', 'bib_fonctions.php');
//variable utilisée pour debugger certaines erreurs
$error_message = '';
if (isset($_POST['btnNouveau'])) {
    goToBlogMaj(False);
} else {
    if (isset($_POST['btnLogin'])) {
        //echo 'btnLogin';
        verifyUserData();
    }
}

htmlInit('StarBlags - Login');

showBandeauPagePublique();
showLoginErrorMessage();
//echo $error_message;
showPied();

htmlEnd();

ob_end_flush();

function showLoginErrorMessage() {
    echo '<div id="blcContenu">
            <h1>Erreur d\'identification</h1>
                <div style="align: center; padding: 10px; height: 200px;">
                    <p>Le pseudo et le mot de passe fourni ne correspondent pas &agrave; un blog.</p>
                    <p>Merci de r&eacute;essayer avec les identifiants corrects.</p>
                </div>
          </div>';
}

function goToBlogMaj($update)
{
    $_SESSION['update'] = $update;
    header("Location: blog_maj.php");
    exit();
}

/**
 * Verifie les donnees utilisteur contenues dans _POST
 */
function verifyUserData() {
    global $error_message;
    if(!isset($_POST['txtPseudo']) || !isset($_POST['txtPasse'])){
        $error_message = "txtPseudo ou txtPasse not set";
        return;
    }
    $passe = md5($_POST['txtPasse']);
    $sql = 'SELECT blID
            FROM blogs
            WHERE blPseudo LIKE "'.$_POST['txtPseudo'].'" AND blPasse LIKE "'.$passe.'"';
    $result = mysqli_query($GLOBALS['bd'], $sql);
    if ($result === False || mysqli_num_rows($result) == 0) {
        $error_message = $result === False ? 'error mysql_query' : 'No results found';
        $error_message = $error_message.' slq : '.$sql;
        unset($_SESSION['txtPseudo']);
        unset($_SESSION['txtPasse']);
        unset($_SESSION['connected']);
        return;
    } else {
        $_SESSION['txtPseudo'] = $_POST['txtPseudo'];
        $_SESSION['txtPasse'] = $passe;
        $_SESSION['connected'] = True;
        $enr = mysqli_fetch_assoc($result);
        $_SESSION['blID'] = $enr['blID'];
        goToBlogMaj(True);
    }
}
?>