<?php
//inclusion bibliotheque de fonctions
include_once('lib_functions.php');

initiateStartConnect('lib_params.php', 'bib_fonctions.php');

htmlInit('Startblags - Upload');

showBandeau(generateLinksWithText('blog_maj.php', 'Mon blog', 'article_maj.php', 'Nouvel article'));
showContenu();
showPied();

htmlEnd();

ob_end_flush();

function showContenu(){
    $articles = getHtmlCodeForArticles();
    echo '<div id="blcContenu">
		    <div id="blcMajTitre">
                Les articles de mon blog...
            </div>
            <table class="majTable" cellspacing="2" cellpadding="4">
                <tr><td class="liste_titre">Titre</td><td class="liste_titre">Date de cr&eacute;ation</td><td class="liste_titre">Heure</td></tr>'.
        $articles.
        '</table>
          </div>';
}

function getHtmlCodeForArticles() {
    $str = '';
    $sql = "SELECT arID, arTitre, DATE_FORMAT(arDate, '%d/%m/%y') AS formatted_date, arHeure
            FROM articles
            WHERE arIDBlog = $_SESSION[blID]";
    $result = mysqli_query($GLOBALS['bd'], $sql) or bdErreur($GLOBALS['bd'], $sql);

    while($enr = mysqli_fetch_assoc($result)) {
        htmlProteger($enr);
        $encrypted_and_hashed_id = encryptAndHash($enr['arID']);
        $str .=
            '<tr onmouseover="FP.listeOver(this, true)" onmouseout="FP.listeOver(this, false)"
			onclick="location.replace(\'article_maj.php?x='.$encrypted_and_hashed_id.'\')">
			    <td>'.$enr['arTitre']."</td>
			    <td>$enr[formatted_date]</td>
			    <td>$enr[arHeure]</td>
            </tr>";
    }
    return  $str;
}
?>