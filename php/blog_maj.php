<?php
//inclusion bibliotheque de fonctions
include_once('lib_functions.php');

initiateStartConnect('lib_params.php', 'bib_fonctions.php');

htmlInit('Startblags - Blog');

$error_messages = array();

if (!isset($_SESSION['update'])){
    echo 'update not set';
    //erreur on est pas passé par login.php
    goToHomePageAndExit();
}
$update = $_SESSION['update'];

$html_to_show = $update ?
    getDefaultLinks()
    : '';
showBandeau($html_to_show);
verifyFieldsAndUpdateDb($error_messages, $update);
showContentCreateOrUpdate($update, $error_messages);

echo '<p></p>';
showPied();

htmlEnd();

ob_end_flush();

/**
 * Verifie les champs du formulaire et si necessaire modifie la base de données.
 * Ajoute les erreurs dans l'array "erreurs"
 * @param $error_messages
 * @param $update
 */
function verifyFieldsAndUpdateDb($error_messages, $update)
{
    if (isset($_POST['btnValider'])) {
        //validation des champs
        $titre = mysqli_real_escape_string($GLOBALS['bd'],  $_POST['blTitre']);
        $resume = mysqli_real_escape_string($GLOBALS['bd'],  $_POST['blResume']);
        $presentation = (int)$_POST['blNbArticlesPage'];
        $ordre = (int)$_POST['blTri'];
        $nom = mysqli_real_escape_string($GLOBALS['bd'],  $_POST['blAuteur']);
        $email = mysqli_real_escape_string($GLOBALS['bd'],  $_POST['blMail']);
        $pseudo = mysqli_real_escape_string($GLOBALS['bd'],  $_POST['blPseudo']);
        $passe = mysqli_real_escape_string($GLOBALS['bd'],  $_POST['blPasse']);
        if (isValidString($passe)) {
            if (isValidLoginOrPass($passe)) {
                $passe = md5($passe);
            } else {
                $passe = '';
                $error_messages [] = 'Le mot passe doit contenir de 4 a 10 caract&egrave;res alphanum&eacute;riques';
            }
        }
        if (isValidString($pseudo)) {
            if (!isValidLoginOrPass($pseudo)) {
                $pseudo = '';
                $error_messages [] = 'Le pseudo doit contenir de 4 a 10 caract&egrave;res alphanum&eacute;riques';
            }
        }
        if (!isValidString($titre)) {
            $error_messages [] = 'Le titre ne peut etre vide.';
        }
        if (!isValidString($resume)) {
            $error_messages [] = 'Le resume ne peut etre vide.';
        }
        if (!isValidString($nom)) {
            $error_messages [] = 'Le nom ne peut etre vide.';
        }
        if (!isValidMail($email)) {
            $error_messages [] = 'L\'adresse mail doit etre valide.';
        }
        if (!is_numeric($presentation) && $presentation > 0) {
            $error_messages [] = 'Le champ nombre de blogs par page est incorrect.';
        }
        if ($ordre > 1 || $ordre < 0) {
            $error_messages [] = 'Le champs tri est incorrect';
        }
        if (count($error_messages) == 0) {
            if ($update) {
                update_blog($error_messages, $titre, $resume, $presentation, $ordre, $nom, $email, $pseudo, $passe);
            } else {
                insert_blog($error_messages, $pseudo, $passe, $titre, $resume, $presentation, $ordre, $nom, $email);
            }
        }
    }
}

/** Insere le blog avec les donnees du formulaire qui suivent et ajoute des messages d'erreur à l'array si des erreurs sont recontrees
 * @param $error_messages
 * @param $pseudo
 * @param $passe
 * @param $titre
 * @param $resume
 * @param $presentation
 * @param $ordre
 * @param $nom
 * @param $email
 */
function insert_blog($error_messages, $pseudo, $passe, $titre, $resume, $presentation, $ordre, $nom, $email)
{
    if (!isValidString($pseudo)) {
        $error_messages [] = 'Pseudo ne peut etre vide';
    }
    if (!isValidString($passe)) {
        $error_messages [] = 'Mot de passe ne peut etre vide';
    }
    if (count($error_messages) > 0) {
        return;
    }
    $date = date("Ymd");
    $heure = date("H:m");
    $sql = 'INSERT INTO blogs (blTitre, blResume, blNbArticlesPage, blTri, blAuteur, blMail, blPseudo, BlPasse, blDate, blHeure)
                        VALUES ("' . $titre . '",' . '"' . $resume . '", "' . $presentation . '", "' . $ordre
        . '", "' . $nom . '", "' . $email . '", "' . $pseudo . '", "' . $passe . '", "' . $date . '", "' . $heure . '")';
    $result = mysqli_query($GLOBALS['bd'], $sql);
    if ($result === False) {
        $error_messages [] = "Erreur create blog : " . $sql;
    } else {
        $_SESSION['IDBlog'] = mysqli_insert_id($GLOBALS['bd']);
        setSessionAndGoToUpload($pseudo, $passe);
    }
}

/**
 * Modifie le blog avec les donnees du formulaire qui suivent et ajoute des messages d'erreur à l'array si des erreurs sont recontrees
 *
 * @param array $error_messages
 * @param string $titre
 * @param string $resume
 * @param int $presentation
 * @param int $ordre
 * @param string $nom
 * @param string $email
 * @param string $pseudo
 * @param string $passe
 */
function update_blog($error_messages, $titre, $resume, $presentation, $ordre, $nom, $email, $pseudo, $passe)
{
    $sql = 'UPDATE blogs
                    SET blTitre=' . '"' . $titre .
        '", blResume=' . '"' . $resume .
        '", blNbArticlesPage=' . $presentation .
        ', blTri=' . $ordre .
        ', blAuteur="' . $nom .
        '", blMail="' . $email . '"';
    if (isValidString($pseudo)) {
        $sql = $sql . ', blPseudo="' . $pseudo . '"';
    }
    if (isValidString($passe)) {
        $sql = $sql . ', blPasse="' . $passe . '"';
    }
    $id = (int)$_SESSION['blID'];
    $sql = "$sql WHERE blID=$id";
    $result = mysqli_query($GLOBALS['bd'], $sql);
    if ($result === False) {
        $error_messages [] = "Erreur update blog : " . $sql;
    } else {
        if (!isValidString($pseudo) || !isValidString($passe)) {
            $sql = "SELECT blPseudo, blPasse
                                FROM blogs
                                WHERE blID=$id";
            $result = mysqli_query($GLOBALS['bd'], $sql);
            if ($result === False) {
                goToHomePageAndExit();
            } else {
                $enr = mysqli_fetch_assoc($result);
                if (is_null($enr)) {
                    goToHomePageAndExit();
                }
                $pseudo = $enr['blPseudo'];
                $passe = $enr['blPasse'];
            }
        }
        setSessionAndGoToUpload($pseudo, $passe);
    }
}

/**
 * Enregistre les variables de session login et mot de passe
 * et revoie a la page upload.php
 * @param string $pseudo
 * @param string $passe (md5)
 */
function setSessionAndGoToUpload($pseudo, $passe)
{
    $_SESSION['txtPseudo'] = $pseudo;
    $_SESSION['txtPasse'] = $passe;
    $_SESSION['uploadType'] = UPLOAD_TYPE_BLOG;
    goToUpload();
}


/**
 * Redirection vers la page 'upload.php'
 */
function goToUpload()
{
    header("Location: upload.php");
    exit();
}


/** Montre le contenu du formulaire
 * @param boolean $update
 * @param $error_messages
 */
function showContentCreateOrUpdate($update, $error_messages) {
    $titre = $update ? 'Je mets &agrave; jour mon blog ...' : 'Je cr&eacute;e mon blog ...';
    $extra_small_text = $update ? '<td class="petit">Laissez la zone vide pour garder votre mot de passe actuel.</td>' : '';
    $bloc_html_erreurs = getErrorMessagesHtmlIfAny($error_messages);
    echo '<div id="blcContenu">
		    <div id="blcMajTitre">' .
        $titre
         . '</div>'.
        $bloc_html_erreurs
		    .'<form method="post" action="blog_maj.php">
		        <table cellspacing="0" cellpadding="2" class="majTable">
		            <tr><td align="right">Titre du blog&nbsp;</td>
		                <td><input type="text" name="blTitre" size="80" maxlength="255" value="" class="saisie"></td></tr>
                    <tr><td align="right" valign="top">R&eacute;sum&eacute;&nbsp;</td>
                        <td><textarea name="blResume" cols="80" rows="6" class="saisie"></textarea></td></tr>
                    <tr><td align="right">Pr&eacute;sentation&nbsp;</td>
                        <td><select name="blNbArticlesPage" size="1"  class="saisie">
                            <option value="1" selected="yes">un article par page</option>
                            <option value="2">deux articles par page</option>
                            <option value="3">trois articles par page</option>
                            <option value="4">quatre articles par page</option></select></td></tr>
                    <tr><td align="right" valign="top">Ordre&nbsp;</td>
                        <td><input type="radio" name="blTri" value="0" checked="true">les articles les plus anciens en premier&nbsp;&nbsp;&nbsp;<br>
                            <input type="radio" name="blTri" value="1">les articles les plus r&eacute;cents en premier&nbsp;&nbsp;&nbsp;</td></tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td align="right">Votre nom&nbsp;</td>
                        <td><input type="text" name="blAuteur" size="80" maxlength="255" value="" class="saisie"></td></tr>
                    <tr><td align="right">E-mail&nbsp;</td><td><input type="text" name="blMail" size="80" maxlength="255" value="" class="saisie"></td></tr>
                    <tr><td align="right">Pseudo&nbsp;</td><td><input type="text" name="blPseudo" size="15" maxlength="10" value="" class="saisie"></td></tr>
                    <tr><td align="right">Passe&nbsp;</td><td><input type="text" name="blPasse" size="15" maxlength="255" value="" class="saisie">'. $extra_small_text . '</td></tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td colspan="2" align="right">&nbsp;&nbsp;<input type="submit" name="btnValider" value="Valider" class="bouton" ></td></tr></table></form></div>';
}

?>