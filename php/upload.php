<?php
//inclusion bibliotheque de fonctions
include_once('lib_functions.php');

initiateStartConnect('lib_params.php', 'bib_fonctions.php');

htmlInit('Startblags - Upload');

showBandeau(getDefaultLinks());
$error_messages = array();
if(isset($_POST['btnUpload'])){
	verifyFileAndUpdateBd();
}
$uploadType = UPLOAD_TYPE_BLOG;
//TODO: extraire de la bse de données
$phNumero = 1;
if(isset($_SESSION['uploadType'])){
	$uploadType = $_SESSION['uploadType'];
}
showContenu($error_messages);
showPied();

htmlEnd();

ob_end_flush();

/**
 * Montre le contenu du formulaire avec tout le code html
 * @param $error_messages
 */
function showContenu($error_messages) {
	global $uploadType;
	$bloc_html_erreurs = getErrorMessagesHtmlIfAny($error_messages);
	$suffixe = $uploadType == UPLOAD_TYPE_BLOG ? 'mon blog' : 'un article';
	echo '<div id="blcContenu">
		    <div id="blcMajTitre">
			T&eacute;l&eacute;charger une photo associ&eacute;e &agrave; '.$suffixe.
		    '</div>'.$bloc_html_erreurs.
		    '<form enctype="multipart/form-data" method="post" action="upload.php">
		        <table cellspacing="0" cellpadding="2" class="majTable">';
	showDefaultUploadForm();
	if($uploadType != UPLOAD_TYPE_BLOG){
		showUploadFormForArticle();
	}
    echo '</table>
            </form></div>';
}

/**
 * Partie du formulaire par defaut commune auxblogs et articles
 */
function showDefaultUploadForm(){
	echo '<tr><td colspan="2">S&eacute;lectionnez un fichier &agrave; t&eacute;l&eacute;charger sur le serveur<br><br>
		                <input type="hidden" name="MAX_FILE_SIZE" value="51200">
		                <input type="file" name="imgFile" size="80"></td></tr>
                    <tr><td colspan="2">&nbsp;</td></tr>
                    <tr><td colspan="2" align="right">&nbsp;&nbsp;<input type="submit" name="btnUpload" value="T&eacute;l&eacute;charger" class="bouton"></td></tr>';
}

function showUploadFormForArticle(){
	$getInputHtmlCode1 = getInputHtmlCode(2, 0, 'en haut', 'phPlace');
	$getInputHtmlCode2 = getInputHtmlCode(2, 1, 'a droite', 'phPlace');
	$getInputHtmlCode3 = getInputHtmlCode(2, 2, 'en bas', 'phPlace');
	$getInputHtmlCode4 = getInputHtmlCode(2, 3, 'a gauche', 'phPlace');
	echo '<tr><td>Image plac&eacute;e</td>
            <td>'.$getInputHtmlCode1.'<br>'. $getInputHtmlCode2 .'<br>'. $getInputHtmlCode3.'<br>'.$getInputHtmlCode4.'</td></tr>';
}

/**
 * Verificaction du fichier uploadé et modifie la base de donnees
 */
function verifyFileAndUpdateBd(){
	global $error_messages, $uploadType, $phNumero;
	$file = $_FILES['imgFile'];
	switch ($file['error']) {
		case 1:
		case 2:
			$error_messages [] = "fichier $file[name] est trop gros: maxi 50Ko";
			break;
		case 3:
			$error_messages [] = "fichier $file[name]: erreur de transfert";
			break;
		case 4:
			$error_messages [] = "fichier $file[name]: non trouvé";
			break;
	}
	if (count($error_messages) > 0) {
		return;
	}
	if (!@is_uploaded_file($file['tmp_name'])) {
		$error_messages [] = "fichier $file[name]: erreur de transfert";
		return;
	}
	$temp = explode('.', $file['name']);
	$extension = $temp[count($temp)-1];
	if (!isValidString($extension) || !($extension === 'png' || $extension === 'jpg' || $extension === 'gif')) {
		$error_messages [] = "fichier $file[name]: erreur d'extension: $extension; il faut soit du png, jpg ou gif ";
		return;
	}

	$upload_blog = $uploadType == UPLOAD_TYPE_BLOG;
	$chemin = $upload_blog ?
		realpath('../').'/upload/'.$_SESSION['blID'].'.'.$extension:
		realpath('../').'/upload/'.$_SESSION['arID']."_ $phNumero".'.'.$extension;
	if(!move_uploaded_file($file['tmp_name'], $chemin)){
		$error_messages [] = "fichier $file[name]: erreur de copie ".$chemin;
		return;
	} else {
		$sql = $upload_blog ?
			"UPDATE blogs
				SET BlPhoto = '$extension'
				WHERE blID = $_SESSION[blID]":
			"UPDATE photos
			SET phIDArticle = $_SESSION[arID],
				phNumero = $phNumero,
				phPlace = $_POST[phPlace],
				phExt='$extension'";
		$result = mysqli_query($GLOBALS['bd'], $sql);
		if(!$result){
			$error_messages [] = "fichier $file[name]: erreur de base de donn&eacute;es";
			return;
		}
		goToArticlesListe();
	}
}

function goToArticlesListe()
{
	header("Location: articles_liste.php");
	exit();
}
?>